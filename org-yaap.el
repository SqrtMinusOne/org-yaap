;;; org-yaap.el --- Yet another alert package for org -*- lexical-binding: t -*-

;; Copyright (C) 2021-2023 Free Software Foundation, Inc.

;; Author: Amy Grinn <grinn.amy@gmail.com>
;;         Stanislav Ochotnický <sochotnicky@gmail.com>
;; Version: 0.2.2
;; File: org-yaap.el
;; Package-Requires: ((emacs "27.1"))
;; Keywords: tools
;; URL: https://gitlab.com/grinn.amy/org-yaap

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Yet another alert package for org mode.
;; Synchronously checks all headings for alerts to send.
;;
;; Enable it with "M-x org-yaap-mode".
;;
;; Customize "org-yaap" in order to see available options.  By
;; default, you will be notified for all scheduled headings
;; (`org-yaap-include-scheduled') and headings with a deadline
;; (`org-yaap-include-deadline') within your agenda files.  If a
;; heading only includes the date, you will be notified at 9am on the
;; day of the heading (`org-yaap-daily-alert').  If you don't mark a
;; heading as done, you will be repeatedly notified every 30 minutes
;; after the heading was due (`org-yaap-overdue-alerts').

;;; Code:

;;;; Requirements

(require 'notifications)
(require 'server)
(require 'subr-x)
(require 'org-element)
(require 'org-clock)
(require 'org-agenda)
(require 'diary-lib)

;;;; Options

(defgroup org-yaap nil "Customization options for org-yaap."
  :group 'applications)

(defcustom org-yaap-daemon-idle-time 5
  "Seconds of idle time to wait for before checking for alerts.
If nil, don't wait for idle time."
  :type '(choice (const :tag "Off" nil)
                 (integer :tag "Seconds")))

(defcustom org-yaap-include-scheduled t
  "Include SCHEDULED headings."
  :type 'boolean)

(defcustom org-yaap-include-deadline t
  "Include headings with a DEADLINE."
  :type 'boolean)

(defcustom org-yaap-alert-title "Agenda"
  "Title for each alert.
Can also be set with the ALERT_TITLE property on a heading to
override this value."
  :type 'string)

(defcustom org-yaap-alert-severity nil
  "Alert severity to use.
This can also be set with the ALERT_SEVERITY property on a
heading to override this value."
  :type '(choice (const :tag "default" nil)
                 (const critical)
                 (const normal)
                 (const low)))

(defcustom org-yaap-alert-timeout -1
  "Alert timeout to use.

Default -1 lets the notification daemon decide how long to keep
the notification shown.  Special value 0 means no timeout and the
notification daemon will not dismiss the alert automatically.
Higher values are milliseconds until alerts are dismissed
automatically.

This can also be set with the ALERT_TIMEOUT property on a heading
to override this value."
  :type '(choice (const :tag "default" -1)
                 (const :tag "no timeout" 0)
                 (integer :tag "milliseconds")))

(defcustom org-yaap-alert-before 0
  "Alert headings this many minutes before due time.

If it is a number, alert this many minutes before (or after for
negative values) either the SCHEDULED or DEADLINE time.  This
does not affect `org-yaap-daily-alert' or ALERT_TIME property.

If it is a list, send an alert for each element that many minutes
before the SCHEDULED or DEADLINE time.

If nil, don't alert for any headlines.

Can also be set with the ALERT_BEFORE property on a heading to
override this value."
  :type '(choice (const :tag "Off" nil)
                 (integer :tag "Once" 0)
                 (repeat :tag "Multiple" (integer :tag "minutes"))))

(defcustom org-yaap-daily-alert 9
  "If non-nil, alert headings without time at this time.
If nil, exclude headings without the time specified.

If it is a number (0-23), alert at the top of that hour.  If it
is a list, the first number is the hour (0-23) and the second
number is the minutes (0-59).

Can also be set with the ALERT_TIME property on a
heading to override this value like

* Alert at 2:30 PM
  SCHEDULED: <2021-10-22 Fri>
  :PROPERTIES:
  :ALERT_TIME: 14 30
  :END:"
  :type '(choice (const :tag "Off" nil)
                 (integer :tag "Hour")
                 (list (integer :tag "Hour" 9) (integer :tag "Minute" 0))))

(defcustom org-yaap-todo-only nil
  "Only alert headings in a `todo' state."
  :type 'boolean)

(defcustom org-yaap-todo-keywords-only nil
  "Only alert headings with the following `todo' keywords."
  :type '(repeat string)
  :group 'org-yaap)

(defcustom org-yaap-exclude-done t
  "Exclude headings in a `done' state."
  :type 'boolean)

(defcustom org-yaap-overdue-alerts 30
  "If non-nil, alert overdue as specified here.

Requires `org-yaap-exclude-done' to be non-nil.

If this is a number, it is the interval at which to resend an
alert after the heading is due but not in a `done' state.

If this is a list, each element represents an alert that will be
sent that many minutes after the heading is due.

Can also be set with the ALERT_OVERDUE property on the heading
to override this value."
  :type '(choice (const :tag "Off" nil)
                 (integer :tag "Interval")
                 (repeat :tag "List" (integer :tag "Minutes"))))

(defcustom org-yaap-include-tags nil
  "Include headings with these tags.

Requires `org-yaap-todo-only' to be non-nil.

List of tags to include on top of any headings in a `todo' state."
  :type '(choice (const :tag "Off" nil)
                 (repeat :tag "Tags" (string :tag "tag"))))

(defcustom org-yaap-exclude-tags nil
  "List of tags to exclude."
  :type '(choice (const :tag "Off" nil)
                 (repeat :tag "Tags" (string :tag "tag"))))

(defcustom org-yaap-only-tags nil
  "If non-nil, only include headings with a tag in this list."
  :type '(choice (const :tag "Off" nil)
                 (repeat :tag "Tags" (string :tag "tag"))))

(defcustom org-yaap-persistent-clock nil
  "If non-nil, show a persistent notification while clocked in.
Respects `org-yaap-todo-only', `org-yaap-exclude-done',
`org-yaap-exclude-tags' and `org-yaap-only-tags'."
  :type 'boolean)

;;;; Variables

(defvar org-yaap-daemon-timer nil "Current timer for org-yaap.")

(defconst org-yaap-persistent-alert-id 599095367
  "The notification id for a persistent alert.")

;;;; org-yaap mode

;;;###autoload
(define-minor-mode org-yaap-mode
  "Toggles sending alert notifications for org mode events."
  :global t
  :lighter " yaap"
  (if org-yaap-mode
      (progn
        (org-yaap-daemon-start)
        (when org-yaap-persistent-clock
          (add-hook 'org-clock-in-hook #'org-yaap--clock-in)
          (add-hook 'org-clock-out-hook #'org-yaap--clock-out)
          (if org-clock-current-task (org-yaap--clock-in))))
    (org-yaap-daemon-cancel)
    (remove-hook 'org-clock-in-hook #'org-yaap--clock-in)
    (remove-hook 'org-clock-out-hook #'org-yaap--clock-out)))

;;;; Commands

(defun org-yaap-daemon-start (&optional timestamp)
  "Check all headings for alerts and set timer.

If TIMESTAMP is nil, will use the current time.  Will send all
alerts now and call this function again at the start of the next
minute.  If `org-yaap-daemon-idle-time' is non-nil, wait until
idle for `org-yaap-daemon-idle-time' seconds before checking."
  (interactive)
  (org-yaap-check timestamp)
  (if org-yaap-daemon-timer (cancel-timer org-yaap-daemon-timer))
  (setq org-yaap-daemon-timer
        (run-with-timer
         (- 60 (string-to-number (format-time-string "%S")))
         nil
         (lambda ()
           (if org-yaap-daemon-idle-time
               (setq org-yaap-daemon-timer
                     (run-with-idle-timer org-yaap-daemon-idle-time
                                          nil
                                          #'org-yaap-daemon-start
                                          (org-timestamp-from-time nil t)))
             (setq org-yaap-daemon-timer nil)
             (org-yaap-daemon-start))))))

(defun org-yaap-daemon-cancel ()
  "Cancel the org-yaap daemon if it is running."
  (interactive)
  (if org-yaap-daemon-timer
      (setq org-yaap-daemon-timer
            (cancel-timer org-yaap-daemon-timer))))

(defun org-yaap-check (&optional timestamp)
  "Send any alerts that should be sent for current minute.
Uses minute from TIMESTAMP if non-nil."
  (interactive)
  (or timestamp (setq timestamp (org-timestamp-from-time nil t)))
  (org-map-entries
   (lambda ()
     (let ((heading (org-element-at-point)))
       (if (org-yaap--should-alert heading timestamp)
           (org-yaap--alert heading :marker (point-marker)))))
   nil 'agenda))

;;;; Utility expressions

(defun org-yaap--check-heading (heading)
  "Determine if any alerts should be sent for HEADING."
  (let ((todo-type (org-element-property :todo-type heading))
        (tags (org-element-property :tags heading)))
    (not (or (and (stringp (org-element-property :title heading))
                  (string-empty-p (org-element-property :title heading)))
             (and org-yaap-todo-only (and (not (eq 'todo todo-type))
                                          (not (seq-intersection org-yaap-include-tags tags))))
             (and org-yaap-todo-keywords-only
                  (not (seq-contains-p
                        org-yaap-todo-keywords-only
                        (substring-no-properties (org-element-property :todo-keyword heading)))))
             (and org-yaap-exclude-done (eq 'done todo-type))
             (seq-intersection org-yaap-exclude-tags tags)
             (and org-yaap-only-tags (not (seq-intersection org-yaap-only-tags tags)))))))

(defun org-yaap--should-alert (heading timestamp)
  "Check if HEADING should be alerted at the minute from TIMESTAMP."
  (let ((scheduled (org-element-property :scheduled heading))
        (deadline (org-element-property :deadline heading))
        due-dates)
    (when (org-yaap--check-heading heading)
      (if (and org-yaap-include-scheduled scheduled) (push scheduled due-dates))
      (if (and org-yaap-include-deadline deadline) (push deadline due-dates))
      (catch 'should-alert
        (dolist (due due-dates)
          (org-yaap--normalize-due heading due timestamp)
          (if (org-element-property :hour-start due)
              (let ((minutes-after (floor (/ (float-time
                                              (time-subtract (org-timestamp-to-time timestamp)
                                                             (org-timestamp-to-time due)))
                                             60))))
                (if (or (member minutes-after (org-yaap--get-alerts heading))
                        (and (> minutes-after 0)
                             org-yaap-exclude-done
                             (not (eq 'diary (org-element-property :type due)))
                             (if-let ((overdue-interval (org-yaap--overdue-interval heading)))
                                 (= 0 (% minutes-after overdue-interval)))))
                    (throw 'should-alert t)))
            (if-let ((daily-alert (org-yaap--daily-alert heading)))
                (and (= (car daily-alert) (org-element-property :hour-start timestamp))
                     (= (cadr daily-alert) (org-element-property :minute-start timestamp))
                     (>= (org-element-property :year-start timestamp) (org-element-property :year-start due))
                     (>= (org-element-property :month-start timestamp) (org-element-property :month-start due))
                     (>= (org-element-property :day-start timestamp) (org-element-property :day-start due))
                     (throw 'should-alert t)))))))))

(defun org-yaap--alert (heading &rest params)
  "Send alert for HEADING.
Various PARAMS can be set:

 :marker     Open marker when clicking on notification.
 :persistent Persist notification until dismissed or replaced.
 :replace    Replace the persistent notification if it exists.
 :title      Set the title explicitly."
  (if (string-suffix-p "Android" (string-trim (shell-command-to-string "uname -a")))
      (apply #'org-yaap--alert-termux heading params)
    (apply #'org-yaap--alert-dbus heading params)))

(defun org-yaap--alert-dbus (heading &rest params)
  "Use the built in `notifications' package to send an alert for HEADING.
PARAMS are described in `org-yaap--alert'."
  (let ((persistent (plist-get params :persistent)))
    (notifications-notify
     :title (or (plist-get params :title) (org-yaap--alert-title heading))
     :body (org-yaap--heading-title heading)
     :urgency (org-yaap--severity heading)
     :actions '("open" "Open")
     :on-action (lambda (id _key)
                  (notifications-close-notification id)
                  (when-let* ((marker (plist-get params :marker))
                              (buffer (marker-buffer marker)))
                    (when (or (and (buffer-live-p buffer) (switch-to-buffer buffer))
                              (and (buffer-file-name buffer) (find-file (buffer-file-name buffer))))
                      (goto-char (marker-position marker))
                      (org-reveal)))
                  (if persistent
                      (run-with-timer
                       0 nil
                       (lambda () (apply #'org-yaap--alert-dbus heading params)))))
     :timeout (if persistent 0 (org-yaap--timeout heading))
     :replaces-id (if (or persistent (plist-get params :replace))
                      org-yaap-persistent-alert-id))))

(defun org-yaap--alert-termux (heading &rest params)
  "Use the `termux-notification' command to send an alert for HEADING.
PARAMS are described in `org-yaap--alert'."
  (let ((persistent (plist-get params :persistent))
        (title (or (plist-get params :title)
                   (org-yaap--alert-title heading))))
    (apply #'start-process "termux-notification" nil
           "termux-notification"
           "--group" "org-yaap"
           "--title" title
           "--content" (org-yaap--heading-title heading)
           (append
            (if persistent '("--ongoing"))
            (if (or persistent (plist-get params :replace))
                (list "--id" (number-to-string org-yaap-persistent-alert-id)))
            (if-let ((priority (org-yaap--severity heading)))
                (cond
                 ((string= priority "critical") '("--priority" "max"))
                 ((string= priority "normal") '("--priority" "default"))
                 ((string= priority "low") '("--priority" "low"))
                 (t '("--priority" "high"))))
            (if-let* ((marker (plist-get params :marker))
                      (file (buffer-file-name (marker-buffer marker)))
                      (pos (marker-position marker)))
                (list "--action"
                      (concat "am startservice "
                              "-a com.termux.RUN_COMMAND "
                              "-e com.termux.RUN_COMMAND_PATH "
                              (if (server-running-p)
                                  (executable-find "emacsclient")
                                (expand-file-name invocation-name invocation-directory))
                              " --esa com.termux.RUN_COMMAND_ARGUMENTS "
                              (if (server-running-p)
                                  (concat (if (> (length (frame-list)) 1)
                                              "--no-wait,"
                                            "--tty,")
                                          "--socket-name," (expand-file-name "server" server-socket-dir) ","))
                              "--eval,'(org-yaap--termux-open \"" file "\" " (number-to-string pos) (if persistent " t " " nil ") "\"" title "\")'"
                              " com.termux/.app.RunCommandService")))))))

(defun org-yaap--termux-open (file pos &optional renotify title)
  "Open FILE at POS and expand context.
If RENOTIFY is non-nil, send the alert again with TITLE."
  (find-file file)
  (goto-char pos)
  (org-reveal)
  (when renotify
    (let ((heading (org-element-at-point))
          (marker (point-marker)))
      (run-with-timer
       1 nil
       (lambda ()
         (org-yaap--alert-termux
          heading
          :marker marker
          :persistent t
          :title title))))))

(defun org-yaap--get-alerts (heading)
  "Get all alerts relative to the due time for HEADING."
  (append
   (if-let ((alert-before (org-element-property :ALERT_BEFORE heading)))
       (if (not (string= "nil" alert-before))
           (mapcar
            (lambda (a) (- (string-to-number a)))
            (split-string alert-before)))
     (if org-yaap-alert-before
         (if (listp org-yaap-alert-before)
             (mapcar #'- org-yaap-alert-before)
           (list (- org-yaap-alert-before)))))
   (if-let ((overdue-alerts (org-element-property :ALERT_OVERDUE heading)))
       (if (and (not (string= "nil" overdue-alerts))
                (> (length (split-string overdue-alerts)) 1))
           (mapcar #'string-to-number (split-string overdue-alerts)))
     (if (and org-yaap-overdue-alerts (listp org-yaap-overdue-alerts))
         org-yaap-overdue-alerts))))

(defun org-yaap--normalize-due (heading due ts)
  "Normalize timestamp DUE according to HEADING and current time TS.

If DUE is a diary sexp, and DUE occurs on the day of TS, set the
month-start, day-start, and year-start properties of DUE to TS.

If HEADING includes a plain timestamp, set the hour-start and
minute-start properties of DUE accordingly."
  (when (not (org-element-property :hour-start due))
    (if (eq 'diary (org-element-property :type due))
        (let ((diary-sexp (and (string-match "<%%\\(([^>\n]+)\\)>"
                                             (org-element-property :raw-value due))
                               (match-string 1 (org-element-property :raw-value due)))))
          (when (diary-sexp-entry diary-sexp
                                  ""
                                  (list
                                   (org-element-property :month-start ts)
                                   (org-element-property :day-start ts)
                                   (org-element-property :year-start ts)))
            (org-element-put-property due :month-start
                                      (org-element-property :month-start ts))
            (org-element-put-property due :day-start
                                      (org-element-property :day-start ts))
            (org-element-put-property due :year-start
                                      (org-element-property :year-start ts)))))
    (when-let* ((txt (org-element-property :raw-value heading))
                (time (and org-agenda-search-headline-for-time
                           (string-match org-plain-time-of-day-regexp txt)
                           (org-get-time-of-day (match-string 1 txt)))))
      (org-element-put-property due :minute-start (% time 100))
      (org-element-put-property due :hour-start (/ time 100)))))

(defun org-yaap--clock-in ()
  "Send persistent notification for clocking in."
  (let ((marker (car org-clock-history)))
    (save-window-excursion
      (with-current-buffer (marker-buffer marker)
        (save-excursion
          (goto-char (marker-position marker))
          (let ((heading (org-element-at-point)))
            (if (org-yaap--check-heading heading)
                (org-yaap--alert heading
                                 :title "Clocked in"
                                 :marker marker
                                 :persistent t))))))))

(defun org-yaap--clock-out ()
  "Send notification for clocking out."
  (let ((marker (car org-clock-history)))
    (save-window-excursion
      (with-current-buffer (marker-buffer marker)
        (save-excursion
          (goto-char (marker-position marker))
          (let ((heading (org-element-at-point)))
            (if (org-yaap--check-heading heading)
                (org-yaap--alert heading
                                 :title "Clocked out"
                                 :marker marker
                                 :replace t))))))))

(defun org-yaap--severity (heading)
  "Get the severity of alerts for HEADING."
  (if-let ((prop (org-element-property :ALERT_SEVERITY heading)))
      (intern prop)
    org-yaap-alert-severity))

(defun org-yaap--timeout (heading)
  "Get the timeout of alerts for HEADING."
  (if-let ((prop (org-element-property :ALERT_TIMEOUT heading)))
      (string-to-number prop)
    org-yaap-alert-timeout))

(defun org-yaap--alert-title (heading)
  "Get the alert title for HEADING."
  (or (org-element-property :ALERT_TITLE heading) org-yaap-alert-title))

(defun org-yaap--heading-title (heading)
  "Strip links from the title of HEADING."
  (with-temp-buffer
    (let ((title (org-element-property :title heading)))
      (if (listp title)
          (if (stringp (car title))
              (setq title (car title))
            (setq title (caddar title))))
      (insert (substring-no-properties title)))
    (goto-char (point-min))
    (while (re-search-forward org-link-bracket-re nil t)
      (let ((desc (match-string-no-properties 2)))
        (delete-region (match-beginning 0) (match-end 0))
        (insert desc)))
    (buffer-string)))

(defun org-yaap--overdue-interval (heading)
  "Get the overdue alert interval for HEADING, if enabled."
  (if-let ((prop (org-element-property :ALERT_OVERDUE heading)))
      (if (and (not (string= "nil" prop))
               (= 1 (length (split-string prop))))
          (string-to-number prop))
    (if (numberp org-yaap-overdue-alerts)
        org-yaap-overdue-alerts)))

(defun org-yaap--daily-alert (heading)
  "Get the daily alert time for HEADING, if enabled."
  (if-let ((prop (org-element-property :ALERT_TIME heading)))
      (if (not (string= "nil" prop))
          (if (= 1 (length (split-string prop)))
              (list (string-to-number prop) 0)
            (mapcar #'string-to-number (split-string prop))))
    (if (numberp org-yaap-daily-alert)
        (list org-yaap-daily-alert 0)
      org-yaap-daily-alert)))

(provide 'org-yaap)

;;; org-yaap.el ends here
