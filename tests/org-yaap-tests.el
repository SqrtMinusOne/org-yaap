;;; org-yaap-tests.el --- Tests for org-yaap -*- lexical-binding: t -*-

(require 'org-yaap)

(defun org-yaap-tests-get-heading (custom-id)
  "Get the heading with CUSTOM-ID from 'org-yaap-tests.org'."
  (find-file "tests/org-yaap-tests.org")
  (let ((inhibit-message t))
    (org-link-open-from-string (format "[[%s]]" custom-id)))
  (org-element-at-point))

(ert-deftest org-yaap-should-alert-before-0 ()
  "Should alert if the timestamp is exactly equal to the scheduled
  time."
  (should (org-yaap--should-alert
           (org-yaap-tests-get-heading "#alert-before-0")
           '(timestamp
             (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 23)))))

(ert-deftest org-yaap-should-not-alert-before-0 ()
  "Should not alert if the timestamp is not equal to the scheduled
  time."
  (should (not (org-yaap--should-alert
                (org-yaap-tests-get-heading "#alert-before-0")
                '(timestamp
                  (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 24))))))

(ert-deftest org-yaap-custom-alert-before ()
  "Should alert if the timestamp matches the ALERT_BEFORE
property of the heading."
  (should (org-yaap--should-alert
           (org-yaap-tests-get-heading "#custom-alert-before")
           '(timestamp
             (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 2)))))

(ert-deftest org-yaap-ignore-scheduled ()
  "If `org-yaap-include-scheduled' is nil, don't alert for
scheduled headings."
  (let ((org-yaap-include-scheduled nil))
    (should (not (org-yaap--should-alert
                  (org-yaap-tests-get-heading "#ignore-scheduled")
                  '(timestamp
                    (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22)))))))

(ert-deftest org-yaap-ignore-deadline ()
  "If `org-yaap-include-deadline' is nil, don't alert for
headings with a deadline."
  (let ((org-yaap-include-deadline nil))
    (should (not (org-yaap--should-alert
                  (org-yaap-tests-get-heading "#ignore-deadline")
                  '(timestamp
                    (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22)))))))

(ert-deftest org-yaap-todo-only-ignore ()
  "Ignore headings without a todo if `org-yaap-todo-only' is
non-nil."
  (let ((org-yaap-todo-only t))
    (should (not (org-yaap--should-alert
                  (org-yaap-tests-get-heading "#todo-only-ignore")
                  '(timestamp
                    (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22)))))))

(ert-deftest org-yaap-todo-only ()
  "If `org-yaap-todo-only' is non-nil, alert on todo heading."
  (let ((org-yaap-todo-only t))
    (should (org-yaap--should-alert
             (org-yaap-tests-get-heading "#todo-only")
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22))))))

(ert-deftest org-yaap-todo-only-include-tags ()
  "Should alert if `org-yaap-todo-only' is non-nil but
`org-yaap-include-tags' includes the tag of the current heading."
  (let ((org-yaap-todo-only t)
        (org-yaap-include-tags '("alert")))
    (should (org-yaap--should-alert
             (org-yaap-tests-get-heading "#todo-only-tag")
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22))))))

(ert-deftest org-yaap-include-done ()
  "Should alert for a completed heading if
`org-yaap-exclude-done' is nil."
  (let ((org-yaap-exclude-done nil))
    (should (org-yaap--should-alert
             (org-yaap-tests-get-heading "#include-done")
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22))))))

(ert-deftest org-yaap-exclude-done ()
  "Should not alert for a completed heading if
`org-yaap-exclude-done is non-nil."
  (should (not (org-yaap--should-alert
                (org-yaap-tests-get-heading "#exclude-done")
                '(timestamp
                  (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22))))))

(ert-deftest org-yaap-only-tags-should-alert ()
  "Should alert if heading has a tag in `org-yaap-only-tags'."
  (let ((org-yaap-only-tags '("alert")))
    (should (org-yaap--should-alert
             (org-yaap-tests-get-heading "#only-tags-should-alert")
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22))))))

(ert-deftest org-yaap-only-tags-should-not-alert ()
  "Should not alert if heading does not have a tag in
`org-yaap-only-tags'."
  (let ((org-yaap-only-tags '("alert")))
    (should (not (org-yaap--should-alert
                  (org-yaap-tests-get-heading "#only-tags-should-not-alert")
                  '(timestamp
                    (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22)))))))

(ert-deftest org-yaap-daily-alert ()
  "Should alert for a heading without time specified if the
timestamp is exactly 9am."
  (let ((heading (org-yaap-tests-get-heading "#daily-alert")))
    (should (org-yaap--should-alert
             heading
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 9 :minute-start 0))))
    (should-not (org-yaap--should-alert
                 heading
                 '(timestamp
                   (:year-start 2021 :month-start 10 :day-start 24 :hour-start 9 :minute-start 1))))))


(ert-deftest org-yaap-custom-alert-time ()
  "Should alert at ALERT_TIME for a heading without a time
specified."
  (should (org-yaap--should-alert
           (org-yaap-tests-get-heading "#alert-time")
           '(timestamp
             (:year-start 2021 :month-start 10 :day-start 24 :hour-start 19 :minute-start 20)))))

(ert-deftest org-yaap-overdue-interval ()
  "Should alert every 30 minutes after a heading is due."
  (let ((heading (org-yaap-tests-get-heading "#overdue-interval")))
    (should (org-yaap--should-alert
             heading
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 30))))
    (should (org-yaap--should-alert
             heading
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 9 :minute-start 0))))))

(ert-deftest org-yaap-overdue-list ()
  "Should alert for each element in ALERT_OVERDUE."
  (let ((heading (org-yaap-tests-get-heading "#overdue-list")))
    (should (org-yaap--should-alert
             heading
             '(timestamp
               (:year-start 2021 :month-start 10 :day-start 24 :hour-start 9 :minute-start 13))))
    (should (org-yaap--should-alert
            heading
            '(timestamp
              (:year-start 2021 :month-start 10 :day-start 24 :hour-start 9 :minute-start 34))))))

(ert-deftest org-yaap-diary-alert ()
  "Should alert for a heading with a diary sexp specified if the
timestamp is exactly 9am."
  (should (org-yaap--should-alert
           (org-yaap-tests-get-heading "#diary-alert")
           '(timestamp
             (:year-start 2022 :month-start 9 :day-start 2 :hour-start 9 :minute-start 0)))))

(ert-deftest org-yaap-should-not-alert-on-wrong-day ()
  "Should not alert if not the same day."
  (should-not (org-yaap--should-alert
               (org-yaap-tests-get-heading "#wrong-day")
               '(timestamp
                 (:year-start 2022 :month-start 9 :day-start 7 :hour-start 15 :minute-start 0)))))

(ert-deftest org-yaap-plain-time ()
  "Should alert at time specified plainly in heading."
  (should (org-yaap--should-alert
           (org-yaap-tests-get-heading "#plain-time")
           '(timestamp
             (:year-start 2022 :month-start 9 :day-start 7 :hour-start 17 :minute-start 38)))))

(ert-deftest org-yaap-plain-diary ()
  "Should alert at time specified plainly in heading at date specified by diary sexp."
  (let ((heading (org-yaap-tests-get-heading "#plain-diary")))
    (should (org-yaap--should-alert
             heading
             '(timestamp
               (:year-start 2022 :month-start 9 :day-start 7 :hour-start 8 :minute-start 49))))
    (should-not (org-yaap--should-alert
                 heading
                 '(timestamp
                   (:year-start 2022 :month-start 9 :day-start 7 :hour-start 9 :minute-start 0))))))

(ert-deftest org-yaap-plain-diary-no-overdue ()
  "Should not alert overdue 30 minutes past plain diary heading."
  (should-not (org-yaap--should-alert
               (org-yaap-tests-get-heading "#plain-diary")
               '(timestamp
                 (:year-start 2022 :month-start 9 :day-start 7 :hour-start 9 :minute-start 19)))))

(ert-deftest org-yaap-todo-keywords ()
  "Should alert with \"FUTURE\" in todo-keywords-only or if the latter is unset."
  (let ((heading (org-yaap-tests-get-heading "#todo-only-keywords-tag"))
        (timestamp '(timestamp
                     (:year-start 2021 :month-start 10 :day-start 24 :hour-start 8 :minute-start 22))))
    (should (org-yaap--should-alert heading timestamp))
    (let ((org-yaap-todo-keywords-only '("TODO" "NEXT")))
      (should-not (org-yaap--should-alert heading timestamp)))
    (let ((org-yaap-todo-keywords-only '("TODO" "FUTURE" "NEXT")))
      (should (org-yaap--should-alert heading timestamp)))))
